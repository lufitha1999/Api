<?php
ob_start();
require_once "conexion/conexion.php";
require_once "respuestas.class.php";

class imagenes extends conexion{

    private $table ="articulos";
    private $token = "";

    public function seeIncomeAnualSp($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["img/es/anual"])){
                    $pagina=  $_GET["img/es/anual"];
                    $listaPacientes = $this-> imagAnualsp($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_fotoesp'],
                
                        );
                    }
                        header("Content-Type: application/json");
                        $res= implode($array);
                        $url = "https://screenermatic.com/screener/filedata/$res"; 
                        $content =$this-> url_get_contents($url);  
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($content).' "/>';
                        http_response_code(200);
                    
                    

                   
                }
            }else{
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("No tiene  acceso a este servicio");
            echo json_encode($datosA);
            }
            }else{
                
        }
    }
}
    private function imagAnualsp($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    public function seeIncomeQuaSp($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["img/es/quater"])){
                    $pagina=  $_GET["img/es/quater"];
                    $listaPacientes = $this-> imagQuaSp($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_fototriesp'],
                
                        );
                    }
                        header("Content-Type: application/json");
                        $res= implode($array);
                        $url = "https://screenermatic.com/screener/filedata/$res"; 
                        $content =$this-> url_get_contents($url);  
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($content).' "/>';
                        http_response_code(200);
                    
                    

                   
                }
            }else{
            
            }
            }else{
            
        }
    }
}
    private function imagQuaSp($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    ////rutas para imagenes en ingles
    public function seeIncomeAnual($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["img/en/anual"])){
                    $pagina=  $_GET["img/en/anual"];
                    $listaPacientes = $this-> imagAnual($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_foto'],
                
                        );
                    }
                        header("Content-Type: application/json");
                        $res= implode($array);
                        $url = "https://screenermatic.com/screener/filedata/$res"; 
                        $content =$this-> url_get_contents($url);  
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($content).' "/>';
                        http_response_code(200);
                    
                    

                   
                }
            }else{
           
            }
            }else{
            
        }
    }
}
    private function imagAnual($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    public function seeIncomeQua($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["img/en/quater"])){
                    $pagina=  $_GET["img/en/quater"];
                    $listaPacientes = $this-> imagQua($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_fototri'],
                
                        );
                    }
                        header("Content-Type: application/json");
                        $res= implode($array);
                        $url = "https://screenermatic.com/screener/filedata/$res"; 
                        $content =$this-> url_get_contents($url);  
                        echo '<img src="data:image/jpeg;base64,'.base64_encode($content).' "/>';
                        http_response_code(200);
                    
                    

                   
                }
            }else{
            
            }
            }else{
            
        }
    }
}
    private function imagQua($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }


    
    private function buscarToken(){
        $query = "SELECT * FROM  clientes WHERE cli_tokenApi = '" . $this->token . "' ";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }
    private function url_get_contents ($url) {
    if (function_exists('curl_exec')){ 
        $conn = curl_init($url);
        curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($conn, CURLOPT_FRESH_CONNECT,  true);
        curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
        $url_get_contents_data = (curl_exec($conn));
        curl_close($conn);
    }elseif(function_exists('file_get_contents')){
        $url_get_contents_data = file_get_contents($url);
    }elseif(function_exists('fopen') && function_exists('stream_get_contents')){
        $handle = fopen ($url, "r");
        $url_get_contents_data = stream_get_contents($handle);
    }else{
        $url_get_contents_data = false;
    }
return $url_get_contents_data;
}
private function obtenerDatosusuario($id){
    $query = "SELECT * FROM clientes WHERE cli_id = '$id' AND fstat = '1' ";
    $datos = parent::obtenerDatos($query);
    if($datos){
        return $datos;
    }else{
        return 0;
    }


}
    
    
}
?>